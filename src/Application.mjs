import express from 'express';
import os from 'os';
import winston from 'winston';

import RootResource from './resource/RootResource.mjs';
import config from './config.mjs';
import manifest from '../package.json';

export default class Application {

  constructor() {
    this.configureLogging();
    process.on('SIGINT', () => process.exit(130));
    process.on('exit', () => this.stop());
  }

  async start() {
    this.expressApp = express();
    winston.info(`\x1b[2m
      \x1b[0m—————————————————————————————————————————————————————————————————————
      ${manifest.description}
      Version: ${manifest.version}
      System Information:
        Platform: Node ${process.version} / ${os.type}
        CPUs: ${os.cpus().length}
        Memory: ${Math.round(os.freemem()/1048576)}M free / ${Math.round(os.totalmem()/1048576)}M total
        PID: ${process.pid}
    `);
    RootResource.instance.attach(this.expressApp);
    this.expressApp.listen(config.port, () => winston.info(`Listening on port ${config.port}`));
  }

  /**
   * Put shutdown hooks here (closing databases, etc.)
   */
  stop() {
    winston.info('Stopping…');
  }

  configureLogging() {
    winston.configure({
      format: winston.format.combine(
        winston.format(info => {
          // Highlight the bad stuff, just a bit
          if (['warn', 'error'].includes(info.level)) {
            info.message = `\x1b[1m${info.message}\x1b[0m`;
          }
          // Add and pretty print additional args, e.g. "winston.info('There was an error', error)"
          const additionalArgs = info[Symbol.for('splat')];
          const replacer = (i, arg) => {
            if (!Array.isArray(arg) && typeof arg === 'object') {
              // The stack and message properties of Errors aren't enumerable, so make a copy that does expose those.
              // To modify the original object would have side effects elsewhere, where we don't want them to serialize
              return {...arg, message: arg.message, stack: arg.stack};
            }
            return arg;
          };
          if (additionalArgs) {
            info.message = `${info.message} ${JSON.stringify(additionalArgs, replacer, 2)}`;
          }
          return info;
        })(),
        winston.format.align(),
        winston.format.colorize(),
        winston.format.timestamp(),
        winston.format.printf(({timestamp, level, message}) => `\x1b[2m${timestamp}\x1b[0m [${level}] ${message}`)
      ),
      level: config.logLevel,
      transports: [new winston.transports.Console()]
    });
    process.on('uncaughtException', (reason, origin) => winston.error('Uncaught exception:', origin, reason));
    process.on('unhandledRejection', (reason, promise) => winston.error('Unhandled rejection:', promise, reason));
    process.on('warning', warning => winston.warn('NodeJS warning:', warning));
  }

}
