import CrudDao from '../../data/abstract/CrudDao.mjs';

/**
 * Generic, pass-through CRUD methods only for use as a starting point to avoid duplication
 */
export default class CrudService {

  /**
   *
   * @param {class} EntityClass the type of object to handle
   * @param {CrudDao} [dao]
   */
  constructor(EntityClass, dao) {
    this.EntityClass = EntityClass;
    this.dao = dao || new CrudDao(EntityClass);
  }

  /**
   *
   * @param {Array<EntityClass>} objects
   * @returns {Promise<void>}
   */
  async createAll(objects) {
    await Promise.all(objects.map(object => this.create(object, object.id)));
  }

  /**
   *
   * @param {EntityClass} object
   * @param {string} id
   * @returns {Promise<void>}
   */
  async create(object, id) {
    await this.dao.create(object, id);
  }

  /**
   *
   * @param {string} id
   * @returns {Promise<EntityClass>}
   */
  async retrieve(id) {
    return this.dao.retrieve(id);
  }

  /**
   *
   * @param {Array<EntityClass>} objects
   * @returns {Promise<void>}
   */
  async updateAll(objects) {
    await Promise.all(objects.map(object => this.update(object, object.id)));
  }

  /**
   *
   * @param {EntityClass} object
   * @param {string} id
   * @returns {Promise<void>}
   */
  async update(object, id) {
    return this.dao.update(object, id);
  }

  /**
   *
   * @param {string} id
   * @returns {Promise<void>}
   */
  async delete(id) {
    return this.dao.delete(id);
  }

  /**
   *
   * @param {object} query
   * @returns {Promise<Array<EntityClass>>}
   */
  async retrieveAll(query) {
    return this.dao.retrieveAll(query);
  }

}
