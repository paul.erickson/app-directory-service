export default {
  baseUrl: process.env.BASE_URL || 'http://localhost:3000',
  dbPath: process.env.DB_PATH || false,
  jwtPublicKeyBase64: process.env.JWT_PUBLIC_KEY_BASE64 || '',
  logLevel: process.env.LOG_LEVEL || 'verbose',
  origins: (process.env.ORIGINS || 'http://localhost:3000,http://127.0.0.1:3000').split(',').map(it => it.trim()),
  port: process.env.PORT || 3000,
  revokedSubjects: (process.env.REVOKED_SUBJECTS || '').split(',').map(it => it.trim()), // TODO: same for jti and iss
  securityDisabled: process.env.SECURITY_DISABLED === 'true'
}
