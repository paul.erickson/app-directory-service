import Collection from './abstract/Collection.mjs';
import Widget from './Widget.mjs';

export default class WidgetCollection extends Collection(Widget) {

  static get mediaSubtype() {
    return `${Widget.mediaSubtype}.collection`;
  }

  constructor(collection) {
    super(collection);
  }

}
