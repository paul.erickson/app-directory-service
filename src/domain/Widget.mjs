import Entity from './abstract/Entity.mjs';
import Schema from './metadata/Schema.mjs';
import config from '../config.mjs';

export default class Widget extends Entity {

  static get schema() {
    if (this._schema === undefined) {
      this._schema = new Schema({
        "$schema": "http://json-schema.org/schema#",
        "$id": `${config.baseUrl}/schema/${this.mediaSubtype}`,
        "base": config.baseUrl,
        "title": "Widget",
        "description": "An entry in the app directory",
        "type": "object",
        "properties": {
          "id": {
            "readonly": true,
            "title": "ID",
            "type": "string"
          },
          "name": {
            "title": "Name",
            "type": "string"
          },
          "fname": {
            "description": "uPortal reference",
            "title": "Functional Name",
            "type": "string"
          },
          "url": {
            "type": "string"
          },
          "icon": {
            "type": "string",
            "default": "star"
          },
          "widget-type": { // TODO: consider typing
            "type": "string",
            "default": "basic",
            "enum": ["basic", "list-of-links", "custom"]
          },
          "widget-config": { // TODO: consider extracting schema or flattening as properties
            "type": "object"
          }
        },
        "required": [
          "name",
          "fname",
          "url"
        ],
        "links": [
          { "title": "Self", "href": "widget/{id}", "rel": "self", "templateRequired": ["id"] },
          { "title": "Collection", "href": "widget", "rel": "collection" }
        ]
      });
    }
    return this._schema;
  }

  constructor(object) {
    super(object);
  }

}
