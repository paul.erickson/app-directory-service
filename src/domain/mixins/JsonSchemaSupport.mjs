import Schema from '../metadata/Schema.mjs';
import config from '../../config.mjs';

/**
 * Mixin to associate JSON Schema information to an object.
 * Classes should override methods they want to customize.
 *
 * @param {class} Superclass
 * @returns
 */
export default (Superclass) => class extends Superclass {

  static get mediaType() {
    return 'application';
  }

  static get mediaSubtype() {
    return `x.uw.${this.name.toString().toLowerCase()}`;
  }

  static get mimeType() {
    const type = this.mediaType;
    const subtype = this.mediaSubtype;
    const suffix = 'json';
    if (this.schema === undefined) {
      return `${type}/${subtype}+${suffix}`;
    }
    const schemaUrl = new URL(`schema/${subtype}`, config.baseUrl);
    const parameters = `profile="${schemaUrl}"`;
    return `${type}/${subtype}+${suffix};${parameters}`;
  }

  /**
   * JSON Schema definition to describe this domain object
   *
   * @returns {Object}
   */
  static get schema() {
    return new Schema({
      "title": this.name,
      "type": "object"
    });
  }

}
