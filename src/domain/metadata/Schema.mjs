/**
 * The JSON Schema specification defines this MIME type, so we won't try to
 * define the schema of the schema, just making this object similar to the
 * JsonSchemaSupport interface so that our header middleware picks it up.
 *
 * @returns {string}
 */
export default class Schema {

  static get mediaType() {
    return 'application';
  }

  static get mediaSubtype() {
    return 'schema';
  }

  static get mimeType() {
    return `${this.mediaType}/${this.mediaSubtype}+json`;
  }

  constructor(object) {
    Object.assign(this, object);
  }

}
