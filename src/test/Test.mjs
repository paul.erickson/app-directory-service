/**
 * Base class to mark a test as something the test runner should run
 *
 * @param Class
 * @returns {*}
 */
export default class Test { }
