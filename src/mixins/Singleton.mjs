export default (Superclass) => class extends Superclass {

  static get instance() {
    if (this._instance === undefined) {
      this._instance = new this();
    }
    return this._instance;
  }

  /**
   * Singleton is expected not to need arguments
   */
  constructor() {
    super();
    if (this.constructor._instance !== undefined) {
      throw `Can't instantiate more than one ${this.constructor.name} because it is a singleton.  Use ${this.constructor.name}.instance`
    }
    this.constructor._instance = this;
  }

}
