import CrudCollectionResource from '../abstract/CrudCollectionResource.mjs'
import CrudService from '../../service/abstract/CrudService.mjs';
import Widget from '../../domain/Widget.mjs';
import WidgetCollection from '../../domain/WidgetCollection.mjs';
import WidgetEntityResource from './WidgetEntityResource.mjs';

export default class WidgetCollectionResource extends CrudCollectionResource {

  constructor(service = new CrudService(Widget)) {
    super(WidgetCollection, service, new WidgetEntityResource(service));
    this.pathSegment = '/widgets?'
  }

}
