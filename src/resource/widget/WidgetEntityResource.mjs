import CrudEntityResource from '../abstract/CrudEntityResource.mjs'
import Widget from '../../domain/Widget.mjs'

export default class WidgetEntityResource extends CrudEntityResource {

  constructor(service) {
    super(Widget, service);
    this.pathSegment = '/:id';
  }

}
