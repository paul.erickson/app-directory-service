import winston from 'winston';

import CollectionResource from '../abstract/CollectionResource.mjs';
import SchemaEntityResource from './SchemaEntityResource.mjs';
import Widget from '../../domain/Widget.mjs';
import WidgetCollection from '../../domain/WidgetCollection.mjs';

export default class SchemaCollectionResource extends CollectionResource {

  constructor() {
    super();
    this.pathSegment = '/schemas?';
    this.resources = [ new SchemaEntityResource() ];
    this.entities = [
      Widget,
      WidgetCollection
    ];
  }

  async retrieveAll() {
    return this.entities.map(entity => entity.schema);
  }

  authorizeRequest(req, next) {
    winston.debug(`Skipping authorization because ${this.constructor.name} is a public resource`);
    next();
  }

}
