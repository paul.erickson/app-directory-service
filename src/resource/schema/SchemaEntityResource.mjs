import winston from 'winston';

import Widget from '../../domain/Widget.mjs';
import WidgetCollection from '../../domain/WidgetCollection.mjs';
import EntityResource from '../abstract/EntityResource.mjs';

export default class SchemaEntityResource extends EntityResource {

  constructor() {
    super();
    this.entities = [
      Widget,
      WidgetCollection
    ];
    this.pathSegment = '/:schemaId'
  }

  /**
   * Override HTTP handler to indicate the cacheability of schemas
   *
   * @private
   */
  async handleGet(req, res) {
    res.header('Cache-Control', 'public, max-age=3600');
    return super.handleGet(req, res);
  }

  async retrieveEntity(id) {
    for (let entity of this.entities) {
      if (id === entity.mediaSubtype) {
        return entity.schema;
      }
    }
  }

  authorizeRequest(req, next) {
    winston.debug(`Skipping authorization because ${this.constructor.name} is a public resource`);
    next();
  }

}
